// Setup the dependencies/packages
const express = require("express");
const mongoose = require("mongoose");

const taskRoute = require("./routes/taskRoute.js")

// Server setup/middlewares

const app = express();
const port = 4000;
app.use(express.json());
app.use(express.urlencoded({extended:true}));

// Add task route
// Allow all the task route created in the task route.js file to use /tasks route

app.use("/task", taskRoute);


// DB Connection
// Connecting to MongoDB Atlas

mongoose.connect("mongodb+srv://admin:admin123@zuitt-bootcamp.rw9m5pa.mongodb.net/B279_to_do?retryWrites=true&w=majority",
{
	useNewUrlParser : true,
	useUnifiedTopology : true

	});



// Server Listening
if(require.main ===module){
	app.listen(port, () => console.log(`Server running at port ${port}`));

}


module.exports = app;



