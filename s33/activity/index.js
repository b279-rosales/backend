// 3
fetch("https://jsonplaceholder.typicode.com/todos")


.then(response => response.json())
.then(result => console.log(result));


// 4
fetch('https://jsonplaceholder.typicode.com/todos')
.then((response) => response.json())
.then((json) => {

	const list = json.map((todo => {
		return todo.title;
	}))

	console.log(list);

});


// 5
fetch("https://jsonplaceholder.typicode.com/todos/1").then(res => res.json())
.then(json => console.log(json));


// 6
fetch("https://jsonplaceholder.typicode.com/todos/1")


.then(response => response.json())
.then((json) => console.log(`The item "${json.title}" on the list has a status of ${json.completed}`));


// 7

fetch("https://jsonplaceholder.typicode.com/todos", {
	method: "POST", 
	headers: {"Content-Type" : "application/json"},
	body: JSON.stringify({
		 "completed": false,
    "id": 1,
    "title": "Created To Do List Item",
    "userId": 1
	})
})

.then(res => res.json())
.then(json => console.log(json));

// 8-9


fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: "PUT", 
	headers: {"Content-Type" : "application/json"},
	body: JSON.stringify({
    "dateCompleted": "Pending",
    "description": "To updatethe my to do list with a different data structure",
    "id": 1,
    "status": "Pending",
    "title": "Updated To Do List Item",
    "userId": 1
	})
})

.then(res => res.json())
.then(json => console.log(json));

// 10

fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: "PATCH", 
	headers: {"Content-Type" : "application/json"},
	body: JSON.stringify({
		title: "Updated To Do List Item"
	})
})

.then(res => res.json())
.then(json => console.log(json));

// 11


fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: "PATCH", 
	headers: {"Content-Type" : "application/json"},
	body: JSON.stringify({
		status: "complete",
		dateCompleted: "03"
	})	
})

.then(res => res.json())
.then(json => console.log(json));

// 12

fetch('https://jsonplaceholder.typicode.com/todos/1', {
  method: 'DELETE',
});
































