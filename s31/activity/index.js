const http = require("http");

const port = 3000;

const server = http.createServer((request, response) => {
	// Accessing the "greeting" route returns a message of "Hello Again"
	// "request" is an object that is sent via the client (browser)
	// The "url" property refers to the url or the link in the browser

	if(request.url == "/login"){
		response.writeHead(200, {"Content-Type" : "text/plain"});
		response.end("They are in the login page.");
	} else {
		response.writeHead(404, {"Content-Type" : "text/plain"});
		response.end("404: Page not found!");
	}
});

server.listen(port);
console.log(`server is successfully running.${port}`);



