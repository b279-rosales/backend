activity.js
1.
db.fruits.aggregate([
  {$match: {onSale: true}},
  {$group: {_id: "$onSale", fruitOnSale: {$sum: 1}}},
  {$project: {_id: 0}}
]);


2.
db.fruits.aggregate([
  {$match: {stock: {$gte: 20}}},
  {$group: {_id: "$supplier_id", enoughStock: {$sum: 1}}},
  {$project: {_id: 0}}
]);


3.
db.fruits.aggregate([
  {$match: {onSale: true}},
  {$group: {_id: "$supplier_id", avg_price: {$sum: "$stock"}}},
  {$sort: {total: 1}}
]);


4.
db.fruits.aggregate([
	{$group: {_id: "$supplier_id" , max_price: { $max: "$price"}}}])

5.
db.fruits.aggregate([
	{$group: {_id: "$supplier_id" , min_price: { $min: "$price"}}}])