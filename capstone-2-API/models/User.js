const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
	email : {
        type : String,
        required : [true, "EMAIL is required!"]
    },
    password : {
        type : String,
        required : [true, "PASSWORD is required!"]
    },
    isAdmin : {
        type : Boolean,
        default : false
    },
    orderedProduct : [
        {
            product :[
                {
                    productId : {
                        type : String,
                        
                    },
                    productName : {
                        type : String,
                        
                    },
                    price : {
                        type : Number
                    },
                    quantity: {
                        type : Number,
                        
                    }
                }
            ],
            subTotal : {
                type : Number
            },
            totalAmount : {
                type : Number,
                
            },
            purchasedOn:{
                type: Date,
                default: new Date()
            }
        }
    ]

})


module.exports = mongoose.model("User", userSchema);
