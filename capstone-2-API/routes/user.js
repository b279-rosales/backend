const express = require('express');
const router = express.Router();
const userController = require("../controllers/userController.js");
const auth = require("../auth.js")

router.post("/checkEmail", (req, res) => {
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
})

// Route for user registration
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
});

// Route for user authentication
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
});

// Route for retrieving user details
router.get("/all", auth.verify, (req, res) => {
    let isAdmin = auth.decode(req.headers.authorization).isAdmin;
    userController.getAllUsers(isAdmin).then(resultFromController => res.send(resultFromController));
});


// Route for create order
router.post("/checkout", auth.verify, (req, res,) => {
    let data = {
        isAdmin : auth.decode(req.headers.authorization).isAdmin,
        userId : auth.decode(req.headers.authorization).id,
        product : req.body
    }
        userController.checkout(data).then(resultFromController => res.send(resultFromController));
});

// Retrieve user details (blank password)
router.post("/details", auth.verify,  (req, res) => {
    const userData = auth.decode(req.headers.authorization)
    userController.getProfile(userData).then(resultFromController => res.send(resultFromController));
});





/// Allows us to export the "router" object that will be access in our index.js file.
module.exports = router;
