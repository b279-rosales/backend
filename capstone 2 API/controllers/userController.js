// The "User" variable is defined using a capitalized letter to indicate that what we are using is the "User" model for code readability
const User = require("../models/User");
const bcrypt = require("bcrypt");
const auth = require("../auth");
const Product = require("../models/Product");

// User registration

module.exports.registerUser = (reqBody) => {

	let newUser = new User({
		email : reqBody.email,
		// 10 is the value provided as the number of "salt" rounds that the bcrypt algorithm will run in order to encrypt the password
		password : bcrypt.hashSync(reqBody.password, 10)

	})

	// Saves the created object to our database
	return newUser.save().then((user, error) => {
		if (error) {

			return false;

		} else {

			return "User registered";

		};

	});

};

// User authentication
module.exports.loginUser = (reqBody) => {

	return User.findOne({email : reqBody.email}).then(result => {

		// User does not exist
		if(result == null){

			return false;

		} else {

			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

			if (isPasswordCorrect) {

				return { access : auth.createAccessToken(result) }

			} else {

				return "Wrong Password";

			};

		};

	});

};

// Funtion to retrieve all users
module.exports.getAllUsers = (isAdmin) => {
	console.log(isAdmin);
	if(isAdmin) {
		return User.find({}).then(result => {
		return result;
	})
		}else{
			let message = Promise.resolve("You don't have the access rights to do this action.");

				return message.then((value) => {
				return value
			})
		}
}




// Create order

module.exports.checkout = async (data, reqBody, isAdmin) => {
    console.log(data.isAdmin);

    if(data.isAdmin){
            return "You're not allowed to order!"
    }else{
        let isUserUpdated = await User.findById(data.userId).then(user => {
            return Product.findById(data.product.productId).then(result =>{
                console.log(result.name)
            let newOrder = {
                products : [{
                    productId : data.product.productId,
                    productName : result.name,
                    quantity : data.product.quantity
                }],
                totalAmount : result.price * data.product.quantity
            }
            user.orderedProduct.push(newOrder);


        console.log(data.product.productId);

        return user.save().then((user, error) => {
            if(error){
                return false;
            }else{
                return true;
            }
        })
            })

    })


    let isProductUpdated = await Product.findById(data.product.productId).then(product => {

        product.userOrders.push({userId: data.userId})

        return product.save().then((product, error) => {
            if(error){
                return false;
            }else{
                return true;
            }
        })

        })


    if(isUserUpdated && isProductUpdated){
        return "Sucessfully Ordered!";
    }else{
        return "Something went wrong with your request. Please try again later!";
    }
    }
}